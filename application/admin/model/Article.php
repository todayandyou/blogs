<?php
/**
 * Created by PhpStorm.
 * User: 33005
 * Date: 2019/2/13
 * Time: 19:07
 */

namespace app\admin\model;

use think\Exception;
use think\Model;

class Article extends Model
{
    protected $name = "cate";

    //初始化
    public function initialize()
    {
        parent::initialize();
    }

    public function deleteArticle($idArray)
    {
        $resultError = 1;
        try {
           $this->startTrans();
            $res = $this->where("id","in",$idArray)->delete();
            if(!$res){
                $resultError = 2;
            }
           $this->commit();
       } catch (Exception $e) {
           $resultError = 2;
           $this->rollback();
       }
       return json(array('data'=>$resultError),200);
    }
    public function cate_find($id){
        return $this->where("id",$id)->find();
    }
    public function edit_cate($data){
        if($data['id']){
            $this->where("id",$data['id'])->update($data);
        }else{
            return false;
        }
    }
    public function cate_list_select(){
        $cate_list = $this
            ->alias("s")
            ->join("cate p","s.parentId = p.id",'left')
            ->field('s.*,p.cateName as parentName')
            ->field(array('s.*','p.cateName'=>'parentName'))
            ->select();
        return $cate_list;
    }


}